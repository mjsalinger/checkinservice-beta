# CheckinService


## Synopsis

An Android Library Intent Service that allows for Point of Interest (POI) querying and persistent check-in storage and retrieval

## Description

 Check-in Intent Service for accessing Point of Interest Data and storing user
 check-in data and stores it in Kinvey's appData repository.  
 
 All requests must contain the following Extras
 		appkey (String)					The Kinvey appkey
 		secret (String)					The Kinvey MasterSecret
 		collection (String)				The Kinvey collection to use
 		
 Possible Intent Actions and extras:
 		getPOI:  Retrieves POI data from Geonames.org.
 			lat (double)				Latitude
 			lon (double)				Longitude
 			Result Action:  			getPOIProcessed
 			Result:  poiList (Parceable ArrayList) ArrayList of POI data
 		checkIn: Check a user into a Point of Interest (POI)
 			poi (Parecable ArrayList)	Point of Interest object
 			username (string)			User to check in
 			Result Action:				checkInProcessed
 			Result:  result (boolean)	Success/Fail of Request
 		getCheckins:  Get user's history of check-ins
 			username (string)			User name of checkins to retrieve
 			Result Action:				getCheckinsProcessed
 			Result:  poiList (Parceable ArrayList) ArrayList of POI data
 		findNearbyPOIsWithCheckins:  Finds POI data in a given radius that has checkin data
 			lat (double)				Latitude
 			lon (double)				Longitude
 			distance (double)			Distance around coordinates to search (0.5 recommended)
 			Result Action				findNearbyPOIsWithCheckinsProcessed
 			Result:  poiList (Parceable ArrayList) ArrayList of POI data  
 
 All calls also return result (boolean) to indicate success or failure, and ErrorMessage containing any errors.

To use, add the CheckinManager Library to your Android project, and set up the Service in your manifest:  

	<service 
            android:enabled="true" 
            android:name="com.msalinger.checkinmanager.CheckinService" />

In your activity, configure a Broadcast Receiver to handle return intents.  The return actions associated with the above calls are:
		getPOI: 			com.msalinger.checkinmanager.CheckinService.getPOIProcessed
		checkIn:			com.msalinger.checkinmanager.CheckinService.checkInProcessed
		getCheckins:			com.msalinger.checkinmanager.CheckinService.getCheckinsProcessed
		findNearbyPOIsWithCheckins:	com.msalinger.checkinmanager.CheckinService.findNearbyPOIsWithCheckinsProcessed

POI objects and ArrayLists of POI objects (included in the library and available via JavaDoc) are used to store and transmit information about each Point of Interest and its checked-in users.  

## Brag about it
The library makes use of an API (geonames) to retrieve and seed location data.  A user can then check in and retrieve their own history of checked-in data, as well as search for nearby checked-in locations.  This can be used by developers to instantiate a location storage process for any location-based games or apps.

By using an IntentService, I was able to extract the code from a UI thread in more of a persistent way than a simple AsyncTask.  The Service calls a LocationManager, which in turn instantiates POI objects and acts as a logic layer.  Communication with the geonames web service and the Kinvey Web Service are both managed by respective classes that inherit from the absrtact RESTService class.

The library makes use of several Kinvey features, including querying records (filters), creating records, and creating and consuming a sequence.  

The framework is modular, expandible, and easy to read through.  While some enhancements and minor tweaks could be made, this library provides the basis for easy expandability and an increased feature set.  
