package com.msalinger.checkinmanagerdemo;

import android.app.Activity;
import android.app.ListFragment;
import android.view.View;
import android.widget.ListView;

public class GeolocListFragment extends ListFragment {
	private ListItemSelectedListener listListener;	
	
	public interface ListItemSelectedListener {
        public void onListItemSelected(int index);
    }

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
	    // TODO Auto-generated method stub
	    super.onListItemClick(l, v, position, id);
	    listListener.onListItemSelected((int)id);
	}
	
	@Override
	public void onAttach(Activity activity) {
		
		super.onAttach(activity);
		
		try {
			listListener = (ListItemSelectedListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnGetPOIListener");
		}
	}
	
}
