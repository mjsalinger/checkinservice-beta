package com.msalinger.checkinmanagerdemo;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class GeolocDetailsFragment extends Fragment {
	private TextSetter textSetter;
	
	public interface TextSetter {
		public String getActivityText();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.geoloc_details_fragment, container, false);
		TextView detailsText = (TextView) view.findViewById(R.id.textPOI);
		
		detailsText.setText(textSetter.getActivityText());
		
		return view;
	}
	
	public void onAttach(Activity activity) {
		
		super.onAttach(activity);
		
		try {
			textSetter = (TextSetter) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnGetPOIListener");
		}
	}
	
}
