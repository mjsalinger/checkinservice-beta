package com.msalinger.checkinmanagerdemo;


import android.os.Bundle;
import android.app.Activity;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class CheckinFragment extends Fragment {
	private CheckinListener checkinListener;
	
	// Definitions
	public interface CheckinListener {
		public void onCheckInClicked();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.checkin_fragment, container, false);

		final Button checkIn = (Button) view.findViewById(R.id.checkIn);
		
		checkIn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				checkinListener.onCheckInClicked();
				
			}
		});
		
		return view;
	}
	
	@Override
	public void onAttach(Activity activity) {
		
		super.onAttach(activity);
		
		try {
			checkinListener = (CheckinListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnGetPOIListener");
		}
	}
	
}
