package com.msalinger.checkinmanagerdemo;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.msalinger.checkinmanager.CheckinService;
import com.msalinger.checkinmanager.POI;

public class MainActivity extends Activity implements CoordinatesFragment.POIListener, GeolocListFragment.ListItemSelectedListener, 
		CheckinFragment.CheckinListener, GeolocDetailsFragment.TextSetter {
	
	private ArrayList<POI> poiList;
	private POI selectedPOI;
	private ArrayAdapter<POI> aa;
	private CheckinReceiver receiver;
	private GeolocListFragment gf;
	private CoordinatesFragment cf;
	private CheckinFragment cif;
	private GeolocDetailsFragment gdf;
	TextView tv;
	
	private final static String INTENT_BASE_URI = "com.msalinger.checkinmanager.CheckinService";
	
	private final static String ACTION_GET_POI = "getPOI";
	private final static String ACTION_CHECK_IN = "checkIn";
	private final static String ACTION_GET_CHECKINS = "getCheckins";
	private final static String ACTION_FIND_NEARBY_POIS_WITH_CHECKINS = "findNearbyPOIsWithCheckins";
		
	private final static String ACTION_GET_POI_PROCESSED = ".getPOIProcessed";
	private final static String ACTION_CHECK_IN_PROCESSED = ".checkInProcessed";
	private final static String ACTION_GET_CHECKINS_PROCESSED = ".getCheckinsProcessed";
	private final static String ACTION_FIND_NEARBY_POIS_WITH_CHECKINS_PROCESSED = ".findNearbyPOIsWithCheckinsProcessed";
	
	private final static String APP_KEY = "kid_TVZbaNqLeJ";
	private final static String MASTER_SECRET = "f7a2b28357d349dc8396fa54a4b6dd2c";
	private final static String COLLECTION = "checkIns";
	
	private final static String KEY_APPKEY ="appkey";
	private final static String KEY_MASTER_SECRET = "secret";
	private final static String KEY_COLLECTION = "collection";
	private final static String KEY_USERNAME = "username";
	private final static String KEY_POI = "poi";
	private final static String KEY_LAT = "lat";
	private final static String KEY_LON = "lon";
	private final static String KEY_DIS = "distance";
	
	private final static String TAG_COORDINATES = "TAG_COORDINATES";
	private final static String TAG_GEOLOC = "TAG_GEOLOC";
	private final static String TAG_CHECKIN = "TAG_CHECKIN";
	private final static String TAG_DETAILS = "TAG_DETAILS";
	
	
	private String username = "msalinger";
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
              
        poiList = new ArrayList<POI>();
        
        gf = new GeolocListFragment();
        gdf = new GeolocDetailsFragment();
        cf = new CoordinatesFragment();
        cif = new CheckinFragment();
        
        inflateCoordinatesdFragment();
		inflateGeolocFragment();
		  
		aa = new ArrayAdapter<POI>(getBaseContext(),android.R.layout.simple_list_item_1, poiList);
		gf.setListAdapter(aa);
       
		
        IntentFilter filter = new IntentFilter(INTENT_BASE_URI + ACTION_GET_POI_PROCESSED);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        filter.addAction(INTENT_BASE_URI + ACTION_CHECK_IN_PROCESSED);
        filter.addAction(INTENT_BASE_URI + ACTION_GET_CHECKINS_PROCESSED);
        filter.addAction(INTENT_BASE_URI + ACTION_FIND_NEARBY_POIS_WITH_CHECKINS_PROCESSED);
        
        receiver = new CheckinReceiver();
        registerReceiver(receiver, filter);
    }

    @Override
   	protected void onResume() {
   		super.onResume();
   		IntentFilter filter = new IntentFilter(INTENT_BASE_URI + ACTION_GET_POI_PROCESSED);
   		filter.addCategory(Intent.CATEGORY_DEFAULT);
   		filter.addAction(INTENT_BASE_URI + ACTION_CHECK_IN_PROCESSED);
   		filter.addAction(INTENT_BASE_URI + ACTION_GET_CHECKINS_PROCESSED);
   		filter.addAction(INTENT_BASE_URI + ACTION_FIND_NEARBY_POIS_WITH_CHECKINS_PROCESSED);

   		registerReceiver(receiver, filter);
   	}
    
    @Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

	private void inflateDetailsFragments() {
		FragmentManager fm = getFragmentManager();
		
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		fragmentTransaction.replace(R.id.action_container, cif, TAG_CHECKIN);
        fragmentTransaction.replace(R.id.fragment_container, gdf, TAG_DETAILS);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
	}
	
	private void inflateGeolocFragment() {
		FragmentManager fm = getFragmentManager();
		
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
        
        fragmentTransaction.add(R.id.fragment_container, gf, TAG_GEOLOC);
        fragmentTransaction.commit();
	}
	
	private void inflateCoordinatesdFragment() {
		FragmentManager fm = getFragmentManager();
		
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
        
        fragmentTransaction.add(R.id.action_container, cf, TAG_COORDINATES);
        fragmentTransaction.commit();
	}

    private String printPOI(POI poi) {
    	String st = "";
    	
		st += "Name: " + poi.getPoiName() +"\n";
		st += "Address: " + poi.getAddress()+"\n";
		st += "City: "+ poi.getCity()+"\n";
		st += "State: " + poi.getState()+"\n";
		st += "Zip: " + poi.getZip()+"\n";
		st += "Lat: " + poi.getLatitude()+"\n";
		st += "Lon: " + poi.getLongitude()+"\n";
		st += "Type: " + poi.getPoiType()+"\n";
		if (poi.getCheckedInUsers() != null) {
			st += "Users:\n";
			for (String user : poi.getCheckedInUsers()) {
				st += "   " + user + "\n";
			}
			st += "\n";	
    	}
    	return st;
    }
    
    public class CheckinReceiver extends BroadcastReceiver {
    	   
    	private final static String INTENT_BASE_URI = "com.msalinger.checkinmanager.CheckinService";
    	
    	private final static String ACTION_GET_POI_PROCESSED = ".getPOIProcessed";
    	private final static String ACTION_CHECK_IN_PROCESSED = ".checkInProcessed";
    	private final static String ACTION_GET_CHECKINS_PROCESSED = ".getCheckinsProcessed";
    	private final static String ACTION_FIND_NEARBY_POIS_WITH_CHECKINS_PROCESSED = ".findNearbyPOIsWithCheckinsProcessed";
    	
    	
    	@Override
    	public void onReceive(Context context, Intent intent) {
    		if (intent.getAction().equals(INTENT_BASE_URI + ACTION_GET_POI_PROCESSED)) {
    			
    			Bundle incomingData = intent.getExtras();
    			
    			if (incomingData.getBoolean("result")) {
    				poiList = incomingData.getParcelableArrayList("poiList");

    				aa = new ArrayAdapter<POI>(getBaseContext(),android.R.layout.simple_list_item_1, poiList);
    				gf.setListAdapter(aa);
    			}
    			else {
    				Toast.makeText(getBaseContext(), "Error: " + incomingData.getString("error"), Toast.LENGTH_LONG).show();
    			}
    			
    	    }
    		else if (intent.getAction().equals(INTENT_BASE_URI + ACTION_CHECK_IN_PROCESSED)) {
    			
    			Bundle incomingData = intent.getExtras();
    			
    			if (incomingData.getBoolean("result")) {
    				Toast.makeText(getBaseContext(), "Check-in Success", Toast.LENGTH_LONG).show();
    			}
    			else {
    				Toast.makeText(getBaseContext(), "Error: " + incomingData.getString("error"), Toast.LENGTH_LONG).show();
    			}

    			
    		}
    		else if (intent.getAction().equals(INTENT_BASE_URI + ACTION_GET_CHECKINS_PROCESSED)) {
    			
    			Bundle incomingData = intent.getExtras();
    			
    			if (incomingData.getBoolean("result")) {
    				poiList = incomingData.getParcelableArrayList("poiList");
    				
    				aa = new ArrayAdapter<POI>(getBaseContext(),android.R.layout.simple_list_item_1, poiList);
    				gf.setListAdapter(aa);
    			}
    			else {
    				Toast.makeText(getBaseContext(), "Error: " + incomingData.getString("error"), Toast.LENGTH_LONG).show();
    			}
    		}
    		else if (intent.getAction().equals(INTENT_BASE_URI + ACTION_FIND_NEARBY_POIS_WITH_CHECKINS_PROCESSED)) {
    			
    			Bundle incomingData = intent.getExtras();
    			
    			if (incomingData.getBoolean("result")) {
    				poiList = incomingData.getParcelableArrayList("poiList");
    				
    				aa = new ArrayAdapter<POI>(getBaseContext(),android.R.layout.simple_list_item_1, poiList);
    				gf.setListAdapter(aa);
    			}
    			else {
    				Toast.makeText(getBaseContext(), "Error: " + incomingData.getString("error"), Toast.LENGTH_LONG).show();
    			}
    		}
    	}
    }


	@Override
	public void onPOIClicked(Double lat, Double lon) {

		 Intent msgIntent = new Intent(this, CheckinService.class);
	     msgIntent.setAction(ACTION_GET_POI);
	     msgIntent.putExtra(KEY_APPKEY,APP_KEY);
	     msgIntent.putExtra(KEY_MASTER_SECRET, MASTER_SECRET);
	     msgIntent.putExtra(KEY_COLLECTION, COLLECTION);
	        
	     msgIntent.putExtra(KEY_LAT, lat);
	     msgIntent.putExtra(KEY_LON,lon);
	     startService(msgIntent);
	}

	@Override
	public void onHistoryClicked() {
		 Intent msgIntent = new Intent(this, CheckinService.class);
	     msgIntent.setAction(ACTION_GET_CHECKINS);
	     msgIntent.putExtra(KEY_APPKEY,APP_KEY);
	     msgIntent.putExtra(KEY_MASTER_SECRET, MASTER_SECRET);
	     msgIntent.putExtra(KEY_COLLECTION, COLLECTION);   
	     msgIntent.putExtra(KEY_USERNAME, username);
	     startService(msgIntent);	
	}

	@Override
	public void onGetAllClicked(Double lat, Double lon, Double distance) {
		 Intent msgIntent = new Intent(this, CheckinService.class);
	     msgIntent.setAction(ACTION_FIND_NEARBY_POIS_WITH_CHECKINS);
	     msgIntent.putExtra(KEY_APPKEY,APP_KEY);
	     msgIntent.putExtra(KEY_MASTER_SECRET, MASTER_SECRET);
	     msgIntent.putExtra(KEY_COLLECTION, COLLECTION);   
	     msgIntent.putExtra(KEY_LAT, lat);
	     msgIntent.putExtra(KEY_LON,lon);
	     msgIntent.putExtra(KEY_DIS, distance);
	     startService(msgIntent);
		
	}
	
	@Override
	public void onCheckInClicked() {
		if (selectedPOI != null) {
			 Intent msgIntent = new Intent(this, CheckinService.class);
		     msgIntent.setAction(ACTION_CHECK_IN);
		     msgIntent.putExtra(KEY_APPKEY,APP_KEY);
		     msgIntent.putExtra(KEY_MASTER_SECRET, MASTER_SECRET);
		     msgIntent.putExtra(KEY_COLLECTION, COLLECTION);   
		        
		     msgIntent.putExtra(KEY_POI, selectedPOI);
		     msgIntent.putExtra(KEY_USERNAME, username);
		     
		     startService(msgIntent);
		 }
		else {
			Toast.makeText(this, "No location found", Toast.LENGTH_SHORT).show();
		}
		
	}

	@Override
	public void onListItemSelected(int index) {
		inflateDetailsFragments();
		
		selectedPOI = poiList.get(index);
	}

	@Override
	public String getActivityText() {
		
		return printPOI(selectedPOI);
	}


}
