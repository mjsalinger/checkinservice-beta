package com.msalinger.checkinmanagerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class CoordinatesFragment extends Fragment {
	// Class Member Declarations
	private POIListener poiListener;
	
	// Definitions
	public interface POIListener {
		public void onPOIClicked(Double lat, Double lon);
		public void onHistoryClicked();
		public void onGetAllClicked(Double lat, Double lon, Double distance);
	}
	
	
	// Superclass Methods
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.coordinates_fragment, container, false);
		
		final Button getPOI = (Button) view.findViewById(R.id.getPOI);
		final Button getHistory = (Button) view.findViewById(R.id.getHistory);
		final Button getAll = (Button) view.findViewById(R.id.getAll);
		
		final EditText editLat = (EditText) view.findViewById(R.id.editLat);
		final EditText editLon = (EditText) view.findViewById(R.id.editLon);
		final EditText editDist = (EditText) view.findViewById(R.id.editDis);
		
		getPOI.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Double lat = 0.0;
				Double lon = 0.0;
				try {
					lat = Double.valueOf(editLat.getText().toString());
					lon = Double.valueOf(editLon.getText().toString());
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), "You must fill in the latitude and longitude.", Toast.LENGTH_LONG).show();
					return;
				}
				poiListener.onPOIClicked(lat, lon);
			}
		});
		
		getHistory.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				poiListener.onHistoryClicked();
			}
		});
		
		getAll.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Double lat = 0.0;
				Double lon = 0.0;
				Double dist = 0.0;
				
				try 
				{
					lat = Double.valueOf(editLat.getText().toString());
					lon = Double.valueOf(editLon.getText().toString());
					dist = Double.valueOf(editDist.getText().toString());
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), "You must fill in the latitude, longitude, and distance.", Toast.LENGTH_LONG).show();
					return;
				}
				
				poiListener.onGetAllClicked(lat, lon, dist);
				
			}
		});
		return view;
	}
	
	@Override
	public void onAttach(Activity activity) {
		
		super.onAttach(activity);
		
		try {
			poiListener = (POIListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnGetPOIListener");
		}
	}
}
