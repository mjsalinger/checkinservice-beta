package com.msalinger.checkinmanager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Base64;
import android.util.Log;

/** 
 * Class for connecting to Kinvey Service.  Extends RESTService and sets common
 * Kinvey parameters and characteristics to the request.  
 * 
 * @author Michael Salinger
 * 
 */
public class KinveyService extends RESTService {
	private final static String TAG = "CHECKINSERVICE";
	private final static String SEQUENCE_ID_KEY = "_id";
	private final static String SEQUENCE_TYPE_KEY = "_type";
	private final static String SEQUENCE_TYPE_VALUE = "KinveySequence";
	private final static String SEQUENCE_VALUE_KEY = "value";
	private final static String SEQUENCE_COLLECTION = "sequences";
	private final static int SEQUENCE_INITIAL_VALUE = 1;
	

	private String appKey;
	private String masterSecret;
	private String authHeader;
	
	/**
	 * Constructor, sets URL, AppKey, MasterSecret, and command for connecting to Kinvey 
	 * REST Web Service.   
	 * 
	 * @param url	URL to connect to Kinvey Web Service
	 * @param cmd	Command to Execute
	 * @param key	appKey
	 * @param secret	MasterSecret
	 */
	public KinveyService(String url, String cmd, String key, String secret) {
		super(url + cmd + key + "/");
		this.appKey=key;
		this.masterSecret = secret;
		this.authHeader = "Basic " + Base64.encodeToString((appKey+":"+masterSecret).getBytes(), Base64.NO_WRAP);
		this.addHeader("Authorization", authHeader);
	}
	
	/**
	 * Executes necessary Kinvey calls to get checkIn info for specific userName
	 * 
	 * @param collectionName	Collection to retrieve from
	 * @param userName			Username to retrieve checkin info for
	 * @return					JSONArray of POIs
	 * @throws JSONException 
	 */
	public JSONArray getCheckedInUsers(String collectionName, String userName) throws JSONException {
		
		try {
			this.url += collectionName + "?query=" + URLEncoder.encode(" { \"checkedInUsers\":\""+userName+"\"}","UTF-8");
		} catch (UnsupportedEncodingException ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return null;
		}
				
		try {
			this.execute(RequestMethod.GET);
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return null;
		}
		return this.getJSONArray();
	}
	
	/**
	 * Executes necessary Kinvey calls to get nearby POIs with checked-in users 
	 * 
	 * @param collectionName	Kinvey collection
	 * @param lat				latitude
	 * @param lon				longitude
	 * @param distance			distance from lat/lon
	 * @return					JSONArray of POIs
	 * @throws JSONException 
	 */
	public JSONArray getNearbyCheckIns(String collectionName, Double lat, Double lon, Double distance) throws JSONException {
		try {
			this.url += collectionName + "?query=" 
					+ URLEncoder.encode("{\"_geoloc\":{\"$nearSphere\":["+String.valueOf(lon)+","+String.valueOf(lat)+"],\"$maxDistance\":\"" 
					+ String.valueOf(distance)+"\"}, \"checkedInUsers\":{\"$ne\":null}}","UTF-8");
		} catch (UnsupportedEncodingException ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			Log.d(TAG,this.url);
			return null;
		}
				
		try {
			this.execute(RequestMethod.GET);
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			Log.d(TAG,this.getResponse());
			return null;
		}
		return this.getJSONArray();
	}
	
	/**
	 * Retrieves next sequence value from a particular Kinvey Sequence
	 * 
	 * @param collectionName	Sequence collection
	 * @param sequenceId		ID of the Sequence
	 * @return					Integer, next sequence number
	 * @throws JSONException 
	 */
	public int getSequence(String collectionName, String sequenceId) throws JSONException {
		
		String baseURL = this.url;
		this.url += SEQUENCE_COLLECTION + "/" + sequenceId;
		
		try {
			this.execute(RequestMethod.GET);
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return -1;
		}
		
		try {
			JSONObject jsonSequenceObject = this.getJSONResponse();
			return jsonSequenceObject.getInt(SEQUENCE_VALUE_KEY);
		} catch (JSONException ex) {
			this.url = baseURL;
			int i=createSequence(collectionName,sequenceId);
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return i;
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return -1;
		}
		
	}
	
	private int createSequence(String collectionName, String sequenceId) throws JSONException {
		this.url += SEQUENCE_COLLECTION;
		this.addHeader("Content-Type", "application/json");
		
		JSONObject newSequence = new JSONObject();
		
		newSequence.put(SEQUENCE_ID_KEY, sequenceId);
		newSequence.put(SEQUENCE_TYPE_KEY, SEQUENCE_TYPE_VALUE);
		newSequence.put(SEQUENCE_VALUE_KEY, SEQUENCE_INITIAL_VALUE);
		
		this.setEntity(newSequence);
		
		try {
			this.execute(RequestMethod.POST);
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return -1;
		}
		return SEQUENCE_INITIAL_VALUE;
	}
	
	/**
	 * Executes necessary Kinvey calls to write a POI to a Kinvey Collection
	 * 
	 * @param collectionName	Collection
	 * @param jsonObject		JSONObject containing the POI to update
	 * @param id				The ID to use
	 * @return					Returns true/false based on success/failure of the update
	 */
	public boolean putJSONObject(String collectionName, JSONObject jsonObject, int id) {
		this.url +=  collectionName + "/" + String.valueOf(id);
		this.addHeader("Content-Type", "application/json");
		this.setEntity(jsonObject);
		try {
			this.execute(RequestMethod.PUT);
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return false;
		}
		return true;
	}

}
