/**
 * 
 */
package com.msalinger.checkinmanager;

import java.util.ArrayList;

import org.json.JSONException;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
/**
 * Check-in Intent Service for accessing Point of Interest Data and storing user
 * check-in data and stores it in Kinvey's appData repository.  
 * 
 * All requests must contain the following Extras
 * 		appkey (String)					The Kinvey appkey
 * 		secret (String)					The Kinvey MasterSecret
 * 		collection (String)				The Kinvey collection to use
 * 		
 * Possible Intent Actions and extras:
 * 		getPOI:  Retrieves POI data.
 * 			lat (double)				Latitude
 * 			lon (double)				Longitude
 * 			Result Action:  			getPOIProcessed
 * 			Result:  poiList (Parceable ArrayList) ArrayList of POI data
 * 		checkIn: Check a user into a Point of Interest (POI)
 * 			poi (Parecable ArrayList)	Point of Interest object
 * 			username (string)			User to check in
 * 			Result Action:				checkInProcessed
 * 			Result:  result (boolean)	Success/Fail of Request
 * 		getCheckins:  Get user's history of check-ins
 * 			username (string)			User name of checkins to retrieve
 * 			Result Action:				getCheckinsProcessed
 * 			Result:  poiList (Parceable ArrayList) ArrayList of POI data
 * 		findNearbyPOIsWithCheckins:  Finds POI data in a given radius that has checkin data
 * 			lat (double)				Latitude
 * 			lon (double)				Longitude
 * 			distance (double)			Distance around coordinates to search (0.5 recommended)
 * 			Result Action				findNearbyPOIsWithCheckinsProcessed
 * 			Result:  poiList (Parceable ArrayList) ArrayList of POI data  
 * 
 * All calls also return result (boolean) to indicate success or failure, and ErrorMessage containing any errors.
 * 	
 * @author Michael Salinger
 *
 */
public class CheckinService extends IntentService {
	private final static String TAG = "CHECKINSERVICE";
	
	private final static String SERVICE_NAME = "CheckinService";
	private final static String BASE_URI = "com.msalinger.checkinmanager.CheckinService";
	private final static String ACTION_GET_POI = "getPOI";
	private final static String ACTION_CHECK_IN = "checkIn";
	private final static String ACTION_GET_CHECKINS = "getCheckins";
	private final static String ACTION_FIND_NEARBY_POIS_WITH_CHECKINS = "findNearbyPOIsWithCheckins";
	private final static String ACTION_GET_POI_PROCESSED = ".getPOIProcessed";
	private final static String ACTION_CHECK_IN_PROCESSED = ".checkInProcessed";
	private final static String ACTION_GET_CHECKINS_PROCESSED = ".getCheckinsProcessed";
	private final static String ACTION_FIND_NEARBY_POIS_WITH_CHECKINS_PROCESSED = ".findNearbyPOIsWithCheckinsProcessed";
	
	private final static String KEY_APPKEY = "appkey";
	private final static String KEY_SECRET = "secret";
	private final static String KEY_COLLECTION = "collection";
	private final static String KEY_USERNAME = "username";
	private final static String KEY_LATITUDE = "lat";
	private final static String KEY_LONGITUDE = "lon";
	private final static String KEY_DISTANCE = "distance";
	private final static String KEY_POI = "poi";
	private final static String OUT_KEY_POILIST = "poiList";
	private final static String OUT_KEY_RESULT = "result";
	private final static String OUT_KEY_ERROR = "error";
		
	public CheckinService() {
		super(SERVICE_NAME);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		
		
		String action = intent.getAction();
        // Data the service was called with.
        Bundle incomingData = intent.getExtras();
        
        String key = incomingData.getString(KEY_APPKEY);
        String secret = incomingData.getString(KEY_SECRET);
        String collection = incomingData.getString(KEY_COLLECTION);

        CheckinManager cm = new CheckinManager(this.getApplicationContext(),key,secret,collection);
        
        Intent broadcastIntent = new Intent();
        
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
 
        
        if (action == ACTION_GET_POI) {
        	Double lat = incomingData.getDouble(KEY_LATITUDE);
        	Double lon = incomingData.getDouble(KEY_LONGITUDE);
        	
        	
        	
	        	ArrayList<POI> nearbyPOIs = new ArrayList<POI>();
	        	//broadcastIntent.setAction(ACTION_GET_POI_PROCESSED);
	        	broadcastIntent.setAction(BASE_URI + ACTION_GET_POI_PROCESSED);
	        	broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        	
	        	
			try {
				
				if (!isLatLonValid(lat,lon)) {
					throw new Exception();
				}
				nearbyPOIs = cm.getPOI(lat, lon);
				
				if (nearbyPOIs != null) {
					broadcastIntent.putExtra(OUT_KEY_RESULT, true);
					broadcastIntent.putExtra(OUT_KEY_ERROR, "");
					broadcastIntent.putParcelableArrayListExtra(OUT_KEY_POILIST, nearbyPOIs);
				}
				else {
					broadcastIntent.putExtra(OUT_KEY_RESULT, false);
					broadcastIntent.putExtra(OUT_KEY_ERROR, "No points of interest found for this location.");
				}
					
			} catch (JSONException ex) {
				Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
				broadcastIntent.putExtra(OUT_KEY_RESULT, false);
				broadcastIntent.putExtra(OUT_KEY_ERROR, ex.getMessage());
			} catch (Exception ex) {
				Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
				broadcastIntent.putExtra(OUT_KEY_RESULT, false);
				broadcastIntent.putExtra(OUT_KEY_ERROR, "Lat must be between -90 and 90; Lon must be between -180 and 180");
			}
        	
        }
        else if (action == ACTION_CHECK_IN) {
        	broadcastIntent.setAction(BASE_URI+ACTION_CHECK_IN_PROCESSED);
        	POI pointOfInterest = (POI) incomingData.getParcelable(KEY_POI);
        	String username = incomingData.getString(KEY_USERNAME);
        	
        	boolean result = cm.checkIn(pointOfInterest, username);    
        	broadcastIntent.putExtra(OUT_KEY_RESULT, result);
        	broadcastIntent.putExtra(OUT_KEY_ERROR, "");
        }
        else if (action ==  ACTION_GET_CHECKINS) {
        	String username = incomingData.getString(KEY_USERNAME);
        	
        	broadcastIntent.setAction(BASE_URI + ACTION_GET_CHECKINS_PROCESSED);
        	ArrayList<POI> checkIns = new ArrayList<POI>();
			try {
				checkIns = cm.getCheckins(username);
				broadcastIntent.putExtra(OUT_KEY_RESULT, true);
				broadcastIntent.putExtra(OUT_KEY_ERROR, "");
				broadcastIntent.putParcelableArrayListExtra(OUT_KEY_POILIST, checkIns);	
			} catch (JSONException ex) {
				Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
				broadcastIntent.putExtra(OUT_KEY_RESULT, false);
				broadcastIntent.putExtra(OUT_KEY_ERROR, ex.getMessage());
			}
        }
        else if (action == ACTION_FIND_NEARBY_POIS_WITH_CHECKINS) {
        	Double lat = incomingData.getDouble(KEY_LATITUDE);
        	Double lon = incomingData.getDouble(KEY_LONGITUDE);
        	Double distance = incomingData.getDouble(KEY_DISTANCE);
        	
        	broadcastIntent.setAction(BASE_URI + ACTION_FIND_NEARBY_POIS_WITH_CHECKINS_PROCESSED);
        	ArrayList<POI> nearbyPOIs = new ArrayList<POI>();
			try {
				nearbyPOIs = cm.findNearbyPOIsWithCheckins(lat, lon, distance);
				broadcastIntent.putExtra(OUT_KEY_RESULT, true);
				broadcastIntent.putExtra(OUT_KEY_ERROR, "");
				broadcastIntent.putParcelableArrayListExtra(OUT_KEY_POILIST, nearbyPOIs);
			} catch (JSONException ex) {
				Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
				broadcastIntent.putExtra(OUT_KEY_RESULT, false);
				broadcastIntent.putExtra(OUT_KEY_ERROR, ex.getMessage());
			}
        	broadcastIntent.putParcelableArrayListExtra(OUT_KEY_POILIST, nearbyPOIs);	
        }    

        sendBroadcast(broadcastIntent);

	}

	private boolean isLatLonValid(Double lat, Double lon) {
		if ((lat >= -90 && lat <= 90) && (lon >=-180 && lat <= 180)) {
			return true;
		}
		else {
			return false;
		}
	}

}
