
package com.msalinger.checkinmanager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


/**
 * RESTService Access Abstract Class
 * @author Michael Salinger
 *
 */
public abstract class RESTService {
	/**
	 * 
	 * ENum containing Request Method Verbs
	 *
	 */
	protected enum RequestMethod {
		GET,
		POST,
		PUT,
		DELETE
	}
	
	private final static String TAG = "CHECKINSERVICE";
	private final static String ERROR_MESSAGE = "error";
	
	private ArrayList<NameValuePair> params;
	private ArrayList<NameValuePair> headers;
	
	protected String url;
	
	private int responseCode;
	private String message;
	
	private String response;
	
	private JSONObject jsonObj; 
	private JSONArray jsonArray;

	/**
	 * @return  Retrieves HTTP Response text
	 *
	 */
	protected String getResponse() {
	    return response;
	}
	/**
	 * 
	 * @return Retrieves HTTP Error Message
	 */
	protected String getErrorMessage() {
	    return message;
	}
	
	/**
	 * 
	 * @return Retrieves Response Code
	 */
	protected int getResponseCode() {
	    return responseCode;
	}
	
	/**
	 * 
	 * @return Retrieves HTTP Response as JSONObject
	 * @throws JSONException 
	 */
	protected JSONObject getJSONResponse() throws JSONException {
		try {
			jsonObj = new JSONObject(response);
		} catch (JSONException ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			jsonObj = new JSONObject();
			try {
				jsonObj.put(ERROR_MESSAGE, ex.getMessage());
			} catch (Exception ex1) {
				Log.d(TAG,ex1.getMessage() + "\n" + ex1.getStackTrace());
				throw ex;
			}
		}
		return jsonObj;
	}
	
	/**
	 * 
	 * @return Retrieves HTTP Response as JSONArray
	 * @throws JSONException 
	 */
	protected JSONArray getJSONArray() throws JSONException {
		try {
			jsonArray = new JSONArray(response);
		} catch (JSONException ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			Log.d(TAG,this.getResponse());
			jsonObj = new JSONObject();
			try {
				jsonArray.put(ex.getMessage());
				return jsonArray;
			} catch (Exception ex1) {
				Log.d(TAG,ex1.getMessage() + "\n" + ex1.getStackTrace());
				throw ex;
			}
		}
		return jsonArray;
	}
	
	/**
	 * Constructor to create a new RESTService Object
	 * 
	 * @param url	URL for object
	 */
	protected RESTService(String url) {
	    this.url = url;
	    params = new ArrayList<NameValuePair>();
	    headers = new ArrayList<NameValuePair>();
	    jsonObj = new JSONObject();
	}
	
	/**
	 * Method to add a parameter to the RequestObject
	 * 
	 * @param name	Name of Parameter
	 * @param value	Value of Parameter
	 */
	protected void addParam(String name, String value) {
	    params.add(new BasicNameValuePair(name, value));
	}
	
	/**
	 * Method to add a header to requestObject
	 * 
	  * @param name	Name of Parameter
	 * @param value	Value of Parameter
	 */
	protected void addHeader(String name, String value) {
	    headers.add(new BasicNameValuePair(name, value));
	}
	
	/**
	 * Sets the JSONObject
	 * 
	 * @param entity
	 */
	protected void setEntity(JSONObject entity) {
		jsonObj = entity;
	}

	/**
	 * Executes the request
	 * 
	 * @param method	Verb to execute 
	 * @throws Exception
	 */
	protected void execute(RequestMethod method) throws Exception {
		switch(method) {
        	case GET: {
	            //add parameters
	            String combinedParams = "";
	            if(!params.isEmpty()) {
	            	combinedParams = addQueryStringParams(combinedParams);
	            }

	            HttpGet request = new HttpGet(url + combinedParams);

	            //add headers 
	            for(NameValuePair h : headers) {
	            	request.addHeader(h.getName(), h.getValue());
	            }
	            url += combinedParams;
	            executeRequest(request);
	            break;
        	}
        	case DELETE: {
        		
        		//add parameters
        		String combinedParams = "";
        		if (!params.isEmpty()) {
        			combinedParams = addQueryStringParams(combinedParams);
        		}
        		HttpDelete request = new HttpDelete(url + combinedParams);
        		addHeaders(request);
        		
        		executeRequest(request);
        		break;
        	
        	}
        	case POST: {
        		HttpPost request = new HttpPost(url);

        		//add headers
        		addHeaders(request);
        		
        		if(!params.isEmpty()) {
        			request.setEntity((HttpEntity) new UrlEncodedFormEntity(params, HTTP.UTF_8));
        		}
        		if (jsonObj != null) {
        			request.setEntity(new ByteArrayEntity(jsonObj.toString().getBytes("UTF8")));
        		}
        		executeRequest(request);
        		break;
        	}
        	case PUT: {
        		HttpPut request = new HttpPut(url);
        		
        		for(NameValuePair h : headers) {
        			request.addHeader(h.getName(), h.getValue());        			
        		}
        		if (!params.isEmpty()) {
        			request.setEntity((HttpEntity) new UrlEncodedFormEntity(params, HTTP.UTF_8));
        		}
        		if (jsonObj != null) {
        			request.setEntity(new ByteArrayEntity(jsonObj.toString().getBytes("UTF8")));
        		}
        		executeRequest(request);
        		break;        		
        	}        
		}
	}

	
	private AbstractHttpMessage addHeaders(AbstractHttpMessage request) {
		for(NameValuePair h : headers) {
			request.addHeader(h.getName(), h.getValue());
		}
		return request;
	}

	private String addQueryStringParams(String combinedParams) throws UnsupportedEncodingException {
		combinedParams += "?";
		for(NameValuePair p : params) {
		    String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(),"UTF-8");
		    if(combinedParams.length() > 1) {
		    	combinedParams  +=  "&" + paramString;
		    }
		    else {
		    	combinedParams += paramString;
		    }
		}
		return combinedParams;
	}


	private void executeRequest(HttpUriRequest request) {
		HttpClient client = new DefaultHttpClient();

		HttpResponse httpResponse;

		try {
			httpResponse = client.execute(request);
			responseCode = httpResponse.getStatusLine().getStatusCode();
			message = httpResponse.getStatusLine().getReasonPhrase();

			HttpEntity entity = httpResponse.getEntity();

			if (entity != null) {

				InputStream instream = entity.getContent();
				response = convertStreamToString(instream);

				// Closing the input stream will trigger connection release
				instream.close();
        }

		} catch (ClientProtocolException ex)  {
			client.getConnectionManager().shutdown();
			Log.d(TAG,ex.getMessage()+"\n"+ex.getStackTrace());
		} catch (IOException ex) {
			client.getConnectionManager().shutdown();
			Log.d(TAG,ex.getMessage()+"\n"+ex.getStackTrace());
		}
	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException ex) {
			Log.d(TAG,ex.getMessage()+"\n"+ex.getStackTrace());
		} finally {
			try {
				is.close();
			} catch (IOException ex) {
				Log.d(TAG,ex.getMessage()+"\n"+ex.getStackTrace());
			}
		}
		return sb.toString();
	}

}