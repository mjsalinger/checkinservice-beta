package com.msalinger.checkinmanager;

import java.util.ArrayList;

import android.content.Context;
import android.util.Log;

import org.json.JSONObject;
import org.json.JSONException;
import org.json.JSONArray;

/** 
 * CheckinManager manages retrieving of Points of Interest and setting of
 * check-ins between multiple web-based APIs.  POIs are retrieved from
 * http://geonames.net and check-in data is stored at http://baas.kinvey.com/
 * <p>
 * Check-in data can be stored in any Kinvey collection using the appKey  
 * and masterSecret provided to you by Kinvey. 
 * 
 * The class allows a user to get a list of POIs near a set of coordinates, 
 * Check into a POI, get my checkin history, and get nearby POIs with Checkin
 * Data.   
 * 
 * @author Michael Salinger
 * 
 */
public class CheckinManager {
	
	// Private Members
	private Context currentContext;
	
	private final static String TAG = "CHECKINSERVICE";
	
	private final static String POIDATA_BASE_URL = "http://api.geonames.org/";
	private final static String POIDATA_COMMAND = "findNearbyPOIsOSMJSON"; 
	private final static String POIDATA_USERNAME = "msalingercheckin";
	private final static String POIDATA_ENTITY = "poi";
	private final static String POIDATA_TYPE = "poiType";
	private final static String POIDATA_NAME = "name";
	private final static String POIDATA_LAT = "lat";
	private final static String POIDATA_LNG = "lng";
	
	private final static String POIDATA_ADDRESS = "address";
	private final static String POIDATA_CITY = "city";
	private final static String POIDATA_STATE = "state";
	private final static String POIDATA_ZIP = "zip";
	
	private final static String POIDATA_CHECKED_IN_USERS = "checkedInUsers";
	private final static String POIDATA_GEOLOC = "_geoloc";
	
	private final static String KINVEY_BASE_URL = "http://baas.kinvey.com/";
	private final static String KINVEY_COMMAND = "appdata/";
	private final static String KINVEY_SEQUENCE_COLLECTION = "sequences";
	private final static String KINVEY_SEQUENCE_ID = "poiId";
	
	private String appKey;
	private String masterSecret;
	private String collectionName;
	
	// Constructors
	
	/**
	 * Constructor for CheckinManager.  Sets authentication information for Kinvey
	 * REST API and provides Context for Geoloc querying.
	 * 
	 * @param context	The current context for retrieving Geoloc data
	 * @param key 		Kinvey appKey for authentication to Kinvey's REST API 
	 * @param secret	MasterSecret for authentication to Kinvey's REST API
	 * @param collection Collection name to store Check-in data
	 */
	public CheckinManager(Context context, String key, String secret, String collection) {
		this.currentContext = context;
		this.appKey = key;
		this.masterSecret = secret;
		this.collectionName = collection;
	}
	
	// Public Methods
	
	/**
	 * Retrieves ArrayList of points of interest objects (POI) 
	 * based on provided latitude/longitude information.
	 * 
	 * @param lat	Latitude of request.  Valid bounds are -90 to 90
	 * @param lon	Longitude of request.  Valid bounts are -180 to 180
	 * @return		Returns an ArrayList of POI objects (com.msalinger.checkinmanager.POI)  
	 * @throws JSONException 
	 */
	public ArrayList<POI> getPOI(double lat, double lon) throws JSONException {
			
		String url = POIDATA_BASE_URL + POIDATA_COMMAND;
		GeonamesService geoSvc = new GeonamesService(url,lat,lon,POIDATA_USERNAME);
		
		JSONObject jsonPoiObject = geoSvc.getPOIs();
		JSONArray jsonPoiArray = new JSONArray();
		try {
			jsonPoiArray = jsonPoiObject.getJSONArray(POIDATA_ENTITY);
		} catch (JSONException ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return null;
		}
		return this.extractPOIfromJSON(jsonPoiArray);   
	}
	
	/**
	 * Checks user in to supplied POI.  
	 * 
	 * @param pointOfInterest	POI object to check into
	 * @param username	Username of user checking in
	 * @return	True if successful, false if not.  
	 */
	public boolean checkIn(POI pointOfInterest, String username) {
		try {
			pointOfInterest.checkIn(username);
			updatePOIService(pointOfInterest);
			return true;
		} catch (Exception ex) {
			Log.d(TAG,ex.getMessage()+ "\n" + ex.getStackTrace());
			return false;
		}
	}
	
	/** 
	 * Retrieves an ArrayList of POI objects that the supplied user has 
	 * checked into.  
	 * 
	 * @param userName	Username of user to search POI data for checkins.
	 * @return	Returns an ArrayList of all POI objects that a user has 
	 * 			checked into.  
	 * @throws JSONException 
	 */
	public ArrayList<POI> getCheckins(String userName) throws JSONException {
				
		KinveyService kinveySvc = new KinveyService(KINVEY_BASE_URL, KINVEY_COMMAND, appKey, masterSecret);
		return this.extractPOIfromJSON(kinveySvc.getCheckedInUsers(collectionName, userName));
		
	}
	
	
	/**
	 * Finds all POIs within 0.5 mile radius of given coordinates.  Returns ArrayList
	 * of POI objects.
	 * 
	 * @param lat	Latitude of request.  Valid bounds are -90 to 90
	 * @param lon	Longitude of request.  Valid bounts are -180 to 180
	 * @return		Returns ArrayList of POI objects
	 * @throws JSONException 
	 */
	public ArrayList<POI> findNearbyPOIsWithCheckins(double lat,double lon) throws JSONException {
		return findNearbyPOIsWithCheckins(lat,lon,0.5);
	}
	
	/**
	 * Finds all POIs within supplied distance (in miles) of given coordinates.  
	 * Returns ArrayList of POI objects.
	 * 
	 * @param lat	Latitude of request.  Valid bounds are -90 to 90
	 * @param lon	Longitude of request.  Valid bounts are -180 to 180
	 * @param distance	Distance in miles to search
	 * @return		Returns ArrayList of POI objects
	 * @throws JSONException 
	 */
	public ArrayList<POI> findNearbyPOIsWithCheckins(double lat,double lon, double distance) throws JSONException {
		
		KinveyService kinveySvc = new KinveyService(KINVEY_BASE_URL, KINVEY_COMMAND, appKey, masterSecret);
		return extractPOIfromJSON(kinveySvc.getNearbyCheckIns(collectionName, lat, lon, distance));
	}
	
	// Private Methods
	
	/**
	 * Extracts POI data from JSON
	 * 
	 * @param jsonPoiArray	JSON Array of POI data
	 * @return ArrayList of POI objects
	 */
	private ArrayList<POI> extractPOIfromJSON(JSONArray jsonPoiArray) {
		ArrayList<POI> pointsOfInterest = new ArrayList<POI>();
		try {
		
			for (int i = 0; i < jsonPoiArray.length(); i++) {
				JSONObject p = jsonPoiArray.getJSONObject(i);
				ArrayList<String> poiCheckedInUsers = new ArrayList<String>();
				JSONArray checkedInUsersJSON = retrieveInnerJSONArray("checkedInUsers", p);
				if (checkedInUsersJSON != null) {
					for (int j = 0; j < checkedInUsersJSON.length(); j++) {
						poiCheckedInUsers.add(checkedInUsersJSON.getString(j));
					}
				}
				String poiType = retrieveJSONString(POIDATA_TYPE, p);
				String poiName = retrieveJSONString(POIDATA_NAME, p);
				String poiAddress = retrieveJSONString(POIDATA_ADDRESS, p);
				String poiCity = retrieveJSONString(POIDATA_CITY, p);
				String poiState = retrieveJSONString(POIDATA_STATE, p);
				String poiZip = retrieveJSONString(POIDATA_ZIP, p);
				Double poiLatitude = p.getDouble(POIDATA_LAT);
				Double poiLongitude = p.getDouble(POIDATA_LNG);
				POI poi = new POI(poiLatitude,poiLongitude,this.currentContext,poiName,poiAddress,poiCity,poiState,poiZip,poiType,poiCheckedInUsers);
				pointsOfInterest.add(poi);

			}
		} catch (JSONException ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
			return null;
		}
		return pointsOfInterest;
	}
	
	/**
	 * Retrieves string from JSON and checks for key's existence. 
	 * 
	 * @param key	Key to retrieve in JSON Object
	 * @param j	JSON Object
	 * @return	String
	 * @throws JSONException
	 */
	private String retrieveJSONString(String key, JSONObject j) throws JSONException {
		if (j.has(key)) {
			return j.getString(key);
		}
		else {
			return null;
		}
	}
	
	/**
	 * Retrieves an Inner JSON Array from a JSON Object and checks for Key's existence.  
	 * 
	 * @param key	Key to retrieve
	 * @param j		JSON Object
	 * @return		JSONArray Object
	 * @throws JSONException
	 */
	private JSONArray retrieveInnerJSONArray(String key, JSONObject j) throws JSONException {
		if (j.has(key)) {
			return j.getJSONArray(key);
		}
		else {
			return null;
		}
	}
	
	/**
	 * Uses Kinvey to generate a sequence
	 * 
	 * @return Next integer sequence from Kinvey
	 * @throws JSONException 
	 */
	private int getSequence() throws JSONException {
		KinveyService kinveySvc = new KinveyService(KINVEY_BASE_URL, KINVEY_COMMAND, appKey, masterSecret);
		return kinveySvc.getSequence(KINVEY_SEQUENCE_COLLECTION, KINVEY_SEQUENCE_ID);		
	}
	
	/**
	 * Updates Kinvey service with POI record
	 * 
	 * @param pointOfInterest POI Object to update
	 * @throws JSONException 
	 */
	private void updatePOIService(POI pointOfInterest) throws JSONException {
		int id = getSequence();  
		
		JSONObject jsonPoiObject = new JSONObject();
		try {
			jsonPoiObject.put(POIDATA_NAME, pointOfInterest.getPoiName());
			jsonPoiObject.put(POIDATA_ADDRESS, pointOfInterest.getAddress());
			jsonPoiObject.put(POIDATA_CITY, pointOfInterest.getCity());
			jsonPoiObject.put(POIDATA_STATE, pointOfInterest.getState());
			jsonPoiObject.put(POIDATA_ZIP, pointOfInterest.getZip());
			jsonPoiObject.put(POIDATA_LAT, pointOfInterest.getLatitude());
			jsonPoiObject.put(POIDATA_LNG, pointOfInterest.getLongitude());
			jsonPoiObject.put("poiType", pointOfInterest.getPoiType());
			ArrayList<Double> latlon = new ArrayList<Double>();
			latlon.add(pointOfInterest.getLongitude());
			latlon.add(pointOfInterest.getLatitude());
			JSONArray geoloc = new JSONArray(latlon);
			jsonPoiObject.put(POIDATA_GEOLOC,geoloc);
			JSONArray jsArray = new JSONArray(pointOfInterest.getCheckedInUsers());
			jsonPoiObject.put(POIDATA_CHECKED_IN_USERS, jsArray);
			
		} catch (JSONException ex) {
			Log.d(TAG,ex.getMessage() + "\n" + ex.getStackTrace());
		}

		KinveyService kinveySvc = new KinveyService(KINVEY_BASE_URL, KINVEY_COMMAND, appKey, masterSecret);
		kinveySvc.putJSONObject(collectionName,jsonPoiObject,id);		
	}
 
}
